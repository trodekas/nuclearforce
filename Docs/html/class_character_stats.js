var class_character_stats =
[
    [ "SetHitLocation", "class_character_stats.html#a852dd1035e98231252ee2fb8a8af864d", null ],
    [ "Start", "class_character_stats.html#a0a9938c06bf9ed6323c178764f0082e0", null ],
    [ "SubtractHealth", "class_character_stats.html#a16f9d8d6156e5badb8c3a96ea448b564", null ],
    [ "currentHealth", "class_character_stats.html#a34f3634662500bdb158a430b73154c67", null ],
    [ "deadBodyPrefab", "class_character_stats.html#a02f0a0ef24db970b2e072fb37b0908d0", null ],
    [ "maxHealth", "class_character_stats.html#a880f299c5eda5eb9c594d22b62c5cfbd", null ],
    [ "movementSpeedGrounded", "class_character_stats.html#a1ba5722da66a0089d0cea729ff2231a3", null ]
];