var class_main_menu_manager =
[
    [ "BackButtonAction", "class_main_menu_manager.html#ac2b24aaaeabe7ac975186bd4b58d3d73", null ],
    [ "ExitButtonAction", "class_main_menu_manager.html#a39bb18eb8eda79c6948ff9912246e107", null ],
    [ "LeaderboardButtonAction", "class_main_menu_manager.html#a005e514c524981ecebba1304afb9ae5b", null ],
    [ "PlayButtonAction", "class_main_menu_manager.html#a76c8544961aef1e7fcbe5747be7a149b", null ],
    [ "ThanksToButtonAction", "class_main_menu_manager.html#a178fa9bd13d3c86d86ac031a501895a7", null ],
    [ "database", "class_main_menu_manager.html#a293cb543fa3fa28f7d8319bb883bacfc", null ],
    [ "leaderboard", "class_main_menu_manager.html#a43bc281dc4aec605b570fcd589b5f4a7", null ],
    [ "leaderboardText", "class_main_menu_manager.html#a33364357a927e3d2a68c89834c3d5222", null ],
    [ "mainMenu", "class_main_menu_manager.html#a8a29ed1934ea9c005bb08674c56495ca", null ],
    [ "playScene", "class_main_menu_manager.html#a9e02d17d3e85c35906a678746dce5263", null ],
    [ "thanksTo", "class_main_menu_manager.html#ac631a931dc163f8b72f281a0985414a6", null ]
];