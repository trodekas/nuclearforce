var class_automatic_weapon =
[
    [ "AllocatePool", "class_automatic_weapon.html#aa68170d7142c4ca0344a53389b3438bc", null ],
    [ "Attack", "class_automatic_weapon.html#a1942bd84dd8f3d4fcd1766322499627d", null ],
    [ "accuracy", "class_automatic_weapon.html#a028bb7c4f9a9d24ea56d9233a859e31a", null ],
    [ "poolSize", "class_automatic_weapon.html#af37fc5c0b3a94793a5c0ee7d05edc4ce", null ],
    [ "projectile", "class_automatic_weapon.html#aa4478122e3de7ef1f11f56f722ac16cf", null ],
    [ "projectileLifeTime", "class_automatic_weapon.html#a65a77fdbae660332db9f46c368f3836f", null ],
    [ "projectileSpeed", "class_automatic_weapon.html#a6fbe171bdee605676f404f49121e3693", null ],
    [ "recoilForceMode", "class_automatic_weapon.html#a619ba1b7216fedf4f69f586d26a1435f", null ],
    [ "recoilStrenghtInAir", "class_automatic_weapon.html#a286a37158db344462b1885c66a16e3fa", null ],
    [ "recoilStrenghtOnGround", "class_automatic_weapon.html#a8a39b8f1e65bc6f3d30f9b91999b523c", null ],
    [ "shootingPoint", "class_automatic_weapon.html#ad84b08f789fa08e0b3601a9e3f3da50a", null ]
];