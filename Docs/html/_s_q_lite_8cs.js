var _s_q_lite_8cs =
[
    [ "SQLiteException", "class_s_q_lite4_unity3d_1_1_s_q_lite_exception.html", "class_s_q_lite4_unity3d_1_1_s_q_lite_exception" ],
    [ "NotNullConstraintViolationException", "class_s_q_lite4_unity3d_1_1_not_null_constraint_violation_exception.html", "class_s_q_lite4_unity3d_1_1_not_null_constraint_violation_exception" ],
    [ "SQLiteConnection", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection" ],
    [ "ColumnInfo", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection_1_1_column_info.html", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection_1_1_column_info" ],
    [ "SQLiteConnectionString", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection_string.html", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection_string" ],
    [ "TableAttribute", "class_s_q_lite4_unity3d_1_1_table_attribute.html", "class_s_q_lite4_unity3d_1_1_table_attribute" ],
    [ "ColumnAttribute", "class_s_q_lite4_unity3d_1_1_column_attribute.html", "class_s_q_lite4_unity3d_1_1_column_attribute" ],
    [ "PrimaryKeyAttribute", "class_s_q_lite4_unity3d_1_1_primary_key_attribute.html", null ],
    [ "AutoIncrementAttribute", "class_s_q_lite4_unity3d_1_1_auto_increment_attribute.html", null ],
    [ "IndexedAttribute", "class_s_q_lite4_unity3d_1_1_indexed_attribute.html", "class_s_q_lite4_unity3d_1_1_indexed_attribute" ],
    [ "IgnoreAttribute", "class_s_q_lite4_unity3d_1_1_ignore_attribute.html", null ],
    [ "UniqueAttribute", "class_s_q_lite4_unity3d_1_1_unique_attribute.html", "class_s_q_lite4_unity3d_1_1_unique_attribute" ],
    [ "MaxLengthAttribute", "class_s_q_lite4_unity3d_1_1_max_length_attribute.html", "class_s_q_lite4_unity3d_1_1_max_length_attribute" ],
    [ "CollationAttribute", "class_s_q_lite4_unity3d_1_1_collation_attribute.html", "class_s_q_lite4_unity3d_1_1_collation_attribute" ],
    [ "NotNullAttribute", "class_s_q_lite4_unity3d_1_1_not_null_attribute.html", null ],
    [ "TableMapping", "class_s_q_lite4_unity3d_1_1_table_mapping.html", "class_s_q_lite4_unity3d_1_1_table_mapping" ],
    [ "Column", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column" ],
    [ "Orm", "class_s_q_lite4_unity3d_1_1_orm.html", "class_s_q_lite4_unity3d_1_1_orm" ],
    [ "SQLiteCommand", "class_s_q_lite4_unity3d_1_1_s_q_lite_command.html", "class_s_q_lite4_unity3d_1_1_s_q_lite_command" ],
    [ "PreparedSqlLiteInsertCommand", "class_s_q_lite4_unity3d_1_1_prepared_sql_lite_insert_command.html", "class_s_q_lite4_unity3d_1_1_prepared_sql_lite_insert_command" ],
    [ "BaseTableQuery", "class_s_q_lite4_unity3d_1_1_base_table_query.html", [
      [ "Ordering", "class_s_q_lite4_unity3d_1_1_base_table_query_1_1_ordering.html", "class_s_q_lite4_unity3d_1_1_base_table_query_1_1_ordering" ]
    ] ],
    [ "Ordering", "class_s_q_lite4_unity3d_1_1_base_table_query_1_1_ordering.html", "class_s_q_lite4_unity3d_1_1_base_table_query_1_1_ordering" ],
    [ "TableQuery", "class_s_q_lite4_unity3d_1_1_table_query.html", "class_s_q_lite4_unity3d_1_1_table_query" ],
    [ "SQLite3", "class_s_q_lite4_unity3d_1_1_s_q_lite3.html", "class_s_q_lite4_unity3d_1_1_s_q_lite3" ],
    [ "Sqlite3DatabaseHandle", "_s_q_lite_8cs.html#a0137d4c5059675164f7580c64441f40c", null ],
    [ "Sqlite3Statement", "_s_q_lite_8cs.html#ab557efa2a7318f6132dd96d8ee66bf7c", null ],
    [ "CreateFlags", "_s_q_lite_8cs.html#a3da1e5d5cbdbfe454cc0d1440f56e7ce", [
      [ "None", "_s_q_lite_8cs.html#a3da1e5d5cbdbfe454cc0d1440f56e7cea6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "ImplicitPK", "_s_q_lite_8cs.html#a3da1e5d5cbdbfe454cc0d1440f56e7cea7f37680f54478a209ab2d2ccd6ee9a2f", null ],
      [ "ImplicitIndex", "_s_q_lite_8cs.html#a3da1e5d5cbdbfe454cc0d1440f56e7cea902eaf6e2e4abeeeb1e901d147777700", null ],
      [ "AllImplicit", "_s_q_lite_8cs.html#a3da1e5d5cbdbfe454cc0d1440f56e7ceaba757689dfce5eb6b5e58418eb82e91b", null ],
      [ "AutoIncPK", "_s_q_lite_8cs.html#a3da1e5d5cbdbfe454cc0d1440f56e7ceaa909006e74071f73d0bf5d2ba878234c", null ]
    ] ],
    [ "SQLiteOpenFlags", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872", [
      [ "ReadOnly", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872a131fb182a881796e7606ed6da27f1197", null ],
      [ "ReadWrite", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872a70a2a84088d405a2e3f1e3accaa16723", null ],
      [ "Create", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872a686e697538050e4664636337cc3b834f", null ],
      [ "NoMutex", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872aed897a216de36c7c934c4baa02b4fea7", null ],
      [ "FullMutex", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872a3322fce9aa781563c086ffd3c13da8b4", null ],
      [ "SharedCache", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872add8141c2dbba4f369c304f748a6badf6", null ],
      [ "PrivateCache", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872a54f0a906e1d7cbe43867c40793c22c91", null ],
      [ "ProtectionComplete", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872af6b459b732ab3369bd8fb1b31d3d019d", null ],
      [ "ProtectionCompleteUnlessOpen", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872ae16febbf8611e3d5db7ed971e2bcb0f7", null ],
      [ "ProtectionCompleteUntilFirstUserAuthentication", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872ad729bc99ed371feb5cc190b05f417172", null ],
      [ "ProtectionNone", "_s_q_lite_8cs.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872a3c2d862f4536788c6b00eb3bbdcd81bf", null ]
    ] ]
];