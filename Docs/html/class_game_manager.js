var class_game_manager =
[
    [ "ActiveDeadUI", "class_game_manager.html#a6ac408e3f2e311290f393ec397d4ca15", null ],
    [ "ActiveTimesUpUI", "class_game_manager.html#a0d22fa64ae45a56c7e42635660d1f1f2", null ],
    [ "IncreaseScore", "class_game_manager.html#a6c726d70c0a4b4759b46393b26fb0604", null ],
    [ "MainMenuButtonAction", "class_game_manager.html#ad04aebbf34dd6a4a9ab19355465d080e", null ],
    [ "RetryButtonAction", "class_game_manager.html#a810ca3d6ef758f7264c5b3f89c1cb7f9", null ],
    [ "database", "class_game_manager.html#aa26eef2da78ddadae265168aedcab620", null ],
    [ "deadUI", "class_game_manager.html#aa9f4c7f91fe3b968ed9db9b7765ea567", null ],
    [ "deathOverlay", "class_game_manager.html#a5e7a79e655b55bc85c656a5e7a3d6248", null ],
    [ "gameOverScoreText", "class_game_manager.html#a72b0ac26c4160dbfb492363ad69f4b8f", null ],
    [ "gameOverTimeText", "class_game_manager.html#a2e4861f7b8abfc5827945e62d0d03c51", null ],
    [ "gamePlayUI", "class_game_manager.html#aa3f63b5a765f9042ee22c9cfe769dfac", null ],
    [ "gamesEndUI", "class_game_manager.html#a8640643cc79c88bb75bca744e3963cc7", null ],
    [ "healthBar", "class_game_manager.html#a31d30832f730172352a51a92d9d3562a", null ],
    [ "jetPackBar", "class_game_manager.html#a4665bb5e05f056ba1fe0476d62622015", null ],
    [ "maxTime", "class_game_manager.html#a102455dbb99d7fdb681986dbd832aeb9", null ],
    [ "score", "class_game_manager.html#ae9711122c3e5251d8f9ff4e02283af09", null ],
    [ "scoreText", "class_game_manager.html#a398d163978e15786ef9cea1be1c877d9", null ],
    [ "timesUpUI", "class_game_manager.html#a580863cc6e9c6ee8f5068289b35862d9", null ],
    [ "timeText", "class_game_manager.html#a7200ad511bdaed28954e6cc9b79d7f56", null ]
];