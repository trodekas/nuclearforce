var hierarchy =
[
    [ "Attribute", null, [
      [ "SQLite4Unity3d.AutoIncrementAttribute", "class_s_q_lite4_unity3d_1_1_auto_increment_attribute.html", null ],
      [ "SQLite4Unity3d.CollationAttribute", "class_s_q_lite4_unity3d_1_1_collation_attribute.html", null ],
      [ "SQLite4Unity3d.ColumnAttribute", "class_s_q_lite4_unity3d_1_1_column_attribute.html", null ],
      [ "SQLite4Unity3d.IgnoreAttribute", "class_s_q_lite4_unity3d_1_1_ignore_attribute.html", null ],
      [ "SQLite4Unity3d.IndexedAttribute", "class_s_q_lite4_unity3d_1_1_indexed_attribute.html", [
        [ "SQLite4Unity3d.UniqueAttribute", "class_s_q_lite4_unity3d_1_1_unique_attribute.html", null ]
      ] ],
      [ "SQLite4Unity3d.MaxLengthAttribute", "class_s_q_lite4_unity3d_1_1_max_length_attribute.html", null ],
      [ "SQLite4Unity3d.NotNullAttribute", "class_s_q_lite4_unity3d_1_1_not_null_attribute.html", null ],
      [ "SQLite4Unity3d.PrimaryKeyAttribute", "class_s_q_lite4_unity3d_1_1_primary_key_attribute.html", null ],
      [ "SQLite4Unity3d.TableAttribute", "class_s_q_lite4_unity3d_1_1_table_attribute.html", null ]
    ] ],
    [ "SQLite4Unity3d.BaseTableQuery", "class_s_q_lite4_unity3d_1_1_base_table_query.html", [
      [ "SQLite4Unity3d.TableQuery< T >", "class_s_q_lite4_unity3d_1_1_table_query.html", null ]
    ] ],
    [ "SQLite4Unity3d.TableMapping.Column", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html", null ],
    [ "SQLite4Unity3d.SQLiteConnection.ColumnInfo", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection_1_1_column_info.html", null ],
    [ "DataService", "class_data_service.html", null ],
    [ "Exception", null, [
      [ "SQLite4Unity3d.SQLiteException", "class_s_q_lite4_unity3d_1_1_s_q_lite_exception.html", [
        [ "SQLite4Unity3d.NotNullConstraintViolationException", "class_s_q_lite4_unity3d_1_1_not_null_constraint_violation_exception.html", null ]
      ] ]
    ] ],
    [ "HighscoreEntrie", "class_highscore_entrie.html", null ],
    [ "IDisposable", null, [
      [ "SQLite4Unity3d.PreparedSqlLiteInsertCommand", "class_s_q_lite4_unity3d_1_1_prepared_sql_lite_insert_command.html", null ],
      [ "SQLite4Unity3d.SQLiteConnection", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html", null ]
    ] ],
    [ "IEnumerable", null, [
      [ "SQLite4Unity3d.TableQuery< T >", "class_s_q_lite4_unity3d_1_1_table_query.html", null ]
    ] ],
    [ "MonoBehaviour", null, [
      [ "AnimationController", "class_animation_controller.html", null ],
      [ "CameraFollow", "class_camera_follow.html", null ],
      [ "CameraShake", "class_camera_shake.html", null ],
      [ "CharacterManager", "class_character_manager.html", null ],
      [ "CharacterMovement", "class_character_movement.html", [
        [ "PlayerMovement", "class_player_movement.html", null ]
      ] ],
      [ "CharacterStats", "class_character_stats.html", [
        [ "PlayerStats", "class_player_stats.html", null ]
      ] ],
      [ "DatabaseManager", "class_database_manager.html", null ],
      [ "DeadBodyManager", "class_dead_body_manager.html", null ],
      [ "EnemyAI", "class_enemy_a_i.html", null ],
      [ "EnemySpawner", "class_enemy_spawner.html", null ],
      [ "EquipmentManager", "class_equipment_manager.html", null ],
      [ "GameManager", "class_game_manager.html", null ],
      [ "JetPack", "class_jet_pack.html", null ],
      [ "MainMenuManager", "class_main_menu_manager.html", null ],
      [ "ParticleSystemManager", "class_particle_system_manager.html", null ],
      [ "PlayerInputManager", "class_player_input_manager.html", null ],
      [ "Projectile", "class_projectile.html", null ],
      [ "StatsBarManager", "class_stats_bar_manager.html", null ],
      [ "WeaponBase", "class_weapon_base.html", [
        [ "AutomaticWeapon", "class_automatic_weapon.html", null ]
      ] ]
    ] ],
    [ "SQLite4Unity3d.BaseTableQuery.Ordering", "class_s_q_lite4_unity3d_1_1_base_table_query_1_1_ordering.html", null ],
    [ "SQLite4Unity3d.Orm", "class_s_q_lite4_unity3d_1_1_orm.html", null ],
    [ "SQLite4Unity3d.SQLite3", "class_s_q_lite4_unity3d_1_1_s_q_lite3.html", null ],
    [ "SQLite4Unity3d.SQLiteCommand", "class_s_q_lite4_unity3d_1_1_s_q_lite_command.html", null ],
    [ "SQLite4Unity3d.SQLiteConnectionString", "class_s_q_lite4_unity3d_1_1_s_q_lite_connection_string.html", null ],
    [ "SQLite4Unity3d.TableMapping", "class_s_q_lite4_unity3d_1_1_table_mapping.html", null ],
    [ "Yielders", "class_yielders.html", null ]
];