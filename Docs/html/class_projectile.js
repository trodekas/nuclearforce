var class_projectile =
[
    [ "maxBloodAmount", "class_projectile.html#a54a2faa5576f6a675672411e5c4a316a", null ],
    [ "minBloodAmount", "class_projectile.html#af1ad4f0136731fc1170970c338e73efa", null ],
    [ "onDestroyEffect", "class_projectile.html#aeb377bc130584fbe86c92190f72e9a86", null ],
    [ "onDestroyLifeTime", "class_projectile.html#a67cc90150dd732e926d73553eb8c7e18", null ],
    [ "projectile", "class_projectile.html#a38a2a2deb2b7d88d50c953cc8ba9e40e", null ],
    [ "projectileLifeTime", "class_projectile.html#a9acca9ebf641322fe32de8fedfb0013e", null ]
];