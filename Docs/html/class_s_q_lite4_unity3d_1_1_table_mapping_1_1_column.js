var class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column =
[
    [ "Column", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#ad1b9988ab9f57655c3c225bc3c2b9ccc", null ],
    [ "GetValue", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#a53cc7fe930cd05f11298de7449efe86f", null ],
    [ "SetValue", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#a2768c810bcb151106d6a9d1cb8fea917", null ],
    [ "Collation", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#addff0fa3c5206f8a0c1e68b909f321ce", null ],
    [ "ColumnType", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#ac0bae39a1b866e633da677522a7c39ad", null ],
    [ "Indices", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#a7d613a3a63c0b93a9354f9184f9ef3c2", null ],
    [ "IsAutoGuid", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#a62abe081870d98a182dc682f9fab2c16", null ],
    [ "IsAutoInc", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#af0b6853243e4a5a59b46c8819f998a91", null ],
    [ "IsNullable", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#a8859834bc9fa0b266563bae893c823fc", null ],
    [ "IsPK", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#a2330ee396ddf292d5a13552236fa89eb", null ],
    [ "MaxStringLength", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#abaaf39157e69d600b8bd8e827efbb941", null ],
    [ "Name", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#a448f60d4d1478e09bf7bfe5afc073897", null ],
    [ "PropertyName", "class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#aff09b856c1b34339acb247d2f10018cf", null ]
];