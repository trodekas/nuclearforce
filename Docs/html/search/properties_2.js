var searchData=
[
  ['collation',['Collation',['../class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#addff0fa3c5206f8a0c1e68b909f321ce',1,'SQLite4Unity3d::TableMapping::Column']]],
  ['columnname',['ColumnName',['../class_s_q_lite4_unity3d_1_1_base_table_query_1_1_ordering.html#a2d00dcf7e97de16c94216caf5a94d537',1,'SQLite4Unity3d::BaseTableQuery::Ordering']]],
  ['columns',['Columns',['../class_s_q_lite4_unity3d_1_1_not_null_constraint_violation_exception.html#a3bca9d3915d432626653953df474959f',1,'SQLite4Unity3d.NotNullConstraintViolationException.Columns()'],['../class_s_q_lite4_unity3d_1_1_table_mapping.html#a7e0cd63dc4aaf88bdcb921f4f9a9063e',1,'SQLite4Unity3d.TableMapping.Columns()']]],
  ['columntype',['ColumnType',['../class_s_q_lite4_unity3d_1_1_table_mapping_1_1_column.html#ac0bae39a1b866e633da677522a7c39ad',1,'SQLite4Unity3d::TableMapping::Column']]],
  ['commandtext',['CommandText',['../class_s_q_lite4_unity3d_1_1_s_q_lite_command.html#aab210e6a92dcdaecd5938caac0225148',1,'SQLite4Unity3d.SQLiteCommand.CommandText()'],['../class_s_q_lite4_unity3d_1_1_prepared_sql_lite_insert_command.html#abb5f6cabe99d123a55e9a87042b9a883',1,'SQLite4Unity3d.PreparedSqlLiteInsertCommand.CommandText()']]],
  ['connection',['Connection',['../class_s_q_lite4_unity3d_1_1_prepared_sql_lite_insert_command.html#a578469d229e970853a109bce9cd07450',1,'SQLite4Unity3d.PreparedSqlLiteInsertCommand.Connection()'],['../class_s_q_lite4_unity3d_1_1_table_query.html#ac65bff9b6888b00ac9f78edd723ef889',1,'SQLite4Unity3d.TableQuery.Connection()']]],
  ['connectionstring',['ConnectionString',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection_string.html#a4908b67ddd45c01f810001dc2c41b2e4',1,'SQLite4Unity3d::SQLiteConnectionString']]]
];
