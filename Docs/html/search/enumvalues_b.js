var searchData=
[
  ['perm',['Perm',['../class_s_q_lite4_unity3d_1_1_s_q_lite3.html#a61e5efd2e550b5fc5db5113f0cc45d5caf263a995390567b6ba090d0a4fb067d2',1,'SQLite4Unity3d::SQLite3']]],
  ['privatecache',['PrivateCache',['../namespace_s_q_lite4_unity3d.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872a54f0a906e1d7cbe43867c40793c22c91',1,'SQLite4Unity3d']]],
  ['protectioncomplete',['ProtectionComplete',['../namespace_s_q_lite4_unity3d.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872af6b459b732ab3369bd8fb1b31d3d019d',1,'SQLite4Unity3d']]],
  ['protectioncompleteunlessopen',['ProtectionCompleteUnlessOpen',['../namespace_s_q_lite4_unity3d.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872ae16febbf8611e3d5db7ed971e2bcb0f7',1,'SQLite4Unity3d']]],
  ['protectioncompleteuntilfirstuserauthentication',['ProtectionCompleteUntilFirstUserAuthentication',['../namespace_s_q_lite4_unity3d.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872ad729bc99ed371feb5cc190b05f417172',1,'SQLite4Unity3d']]],
  ['protectionnone',['ProtectionNone',['../namespace_s_q_lite4_unity3d.html#a7f1ae90ab46e51a51ac5bd8ca2cc0872a3c2d862f4536788c6b00eb3bbdcd81bf',1,'SQLite4Unity3d']]]
];
