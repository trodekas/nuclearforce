var searchData=
[
  ['dataservice',['DataService',['../class_data_service.html#a151f62131295f0c85f6ae45e354ef3b6',1,'DataService']]],
  ['decreaseactiveenemies',['DecreaseActiveEnemies',['../class_enemy_spawner.html#ab1c9a4e7bbee3262e1d9bf7dab479fce',1,'EnemySpawner']]],
  ['deferred',['Deferred',['../class_s_q_lite4_unity3d_1_1_table_query.html#a8548e355a73f7e1bac0161708b2f97cc',1,'SQLite4Unity3d::TableQuery']]],
  ['deferredquery',['DeferredQuery',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#ac46d1ef239960fb8defcff77d1594175',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['deferredquery_3c_20t_20_3e',['DeferredQuery&lt; T &gt;',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a510e6fc55ffc83912b510b591d55e087',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['delete',['Delete',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a9ade3024c2722905f7ed9bf1917adeaa',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['delete_3c_20t_20_3e',['Delete&lt; T &gt;',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a8c0980e1a8f9cac9471607f81ff61085',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['deleteall_3c_20t_20_3e',['DeleteAll&lt; T &gt;',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a95dd327f91b433858bfe22e1ab42a2cf',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['dispose',['Dispose',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a12d7f9cceeb11afb8528d00a443f8f3c',1,'SQLite4Unity3d.SQLiteConnection.Dispose()'],['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#ae6e5f0b978413a2542767e9a1be37297',1,'SQLite4Unity3d.SQLiteConnection.Dispose(bool disposing)'],['../class_s_q_lite4_unity3d_1_1_prepared_sql_lite_insert_command.html#aed9ce1170550f9f254a4210ba40add7a',1,'SQLite4Unity3d.PreparedSqlLiteInsertCommand.Dispose()']]],
  ['droptable_3c_20t_20_3e',['DropTable&lt; T &gt;',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a1c6061939b0edc8f4e56085b173be1e9',1,'SQLite4Unity3d::SQLiteConnection']]]
];
