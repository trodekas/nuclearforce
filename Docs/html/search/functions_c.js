var searchData=
[
  ['oninstancecreated',['OnInstanceCreated',['../class_s_q_lite4_unity3d_1_1_s_q_lite_command.html#ab2a78b9456c70c50270ba10b0f658ca8',1,'SQLite4Unity3d::SQLiteCommand']]],
  ['open',['Open',['../class_s_q_lite4_unity3d_1_1_s_q_lite3.html#a183c2b5f0f2024e1d02d5e6b34c2700c',1,'SQLite4Unity3d.SQLite3.Open([MarshalAs(UnmanagedType.LPStr)] string filename, out IntPtr db)'],['../class_s_q_lite4_unity3d_1_1_s_q_lite3.html#af2c8ee6121814121a7f1d0816b04bf72',1,'SQLite4Unity3d.SQLite3.Open([MarshalAs(UnmanagedType.LPStr)] string filename, out IntPtr db, int flags, IntPtr zvfs)'],['../class_s_q_lite4_unity3d_1_1_s_q_lite3.html#aea5bdfc3d5fe67aee0272d040b96dd02',1,'SQLite4Unity3d.SQLite3.Open(byte[] filename, out IntPtr db, int flags, IntPtr zvfs)']]],
  ['open16',['Open16',['../class_s_q_lite4_unity3d_1_1_s_q_lite3.html#af70615af7ab93f0c30246aeb698cfe43',1,'SQLite4Unity3d::SQLite3']]],
  ['orderby_3c_20u_20_3e',['OrderBy&lt; U &gt;',['../class_s_q_lite4_unity3d_1_1_table_query.html#af85190b51f90beb857af74442241a6ee',1,'SQLite4Unity3d::TableQuery']]],
  ['orderbydescending_3c_20u_20_3e',['OrderByDescending&lt; U &gt;',['../class_s_q_lite4_unity3d_1_1_table_query.html#ad371939550011225258a52c5aa5305f2',1,'SQLite4Unity3d::TableQuery']]]
];
