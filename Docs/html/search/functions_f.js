var searchData=
[
  ['release',['Release',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a579cff32cef7356c2649a21e6d179239',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['reset',['Reset',['../class_s_q_lite4_unity3d_1_1_s_q_lite3.html#a7c47ee06555008f7397b4211f442676a',1,'SQLite4Unity3d::SQLite3']]],
  ['retrybuttonaction',['RetryButtonAction',['../class_game_manager.html#a810ca3d6ef758f7264c5b3f89c1cb7f9',1,'GameManager']]],
  ['rollback',['Rollback',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#adf5ddd8c4d552e45aaad7354522eaf9f',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['rollbackto',['RollbackTo',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a1d32ee1191bb5046137a93856eabe85d',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['runindatabaselock',['RunInDatabaseLock',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a29dc130d4ecfb9337d8ea77c25296b92',1,'SQLite4Unity3d::SQLiteConnection']]],
  ['runintransaction',['RunInTransaction',['../class_s_q_lite4_unity3d_1_1_s_q_lite_connection.html#a141b1ce53970d4ab2dc0025208b62263',1,'SQLite4Unity3d::SQLiteConnection']]]
];
