﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class for managing character movements
/// </summary>
public class CharacterMovement : MonoBehaviour
{
    #region Variables

    [HideInInspector]
    public Rigidbody2D rigidBody;

    private CharacterStats characterStats;
    private Vector2 movementVector = new Vector2(0, 0);

    public float movingSpeedSmoothen = 10f;

    public bool facingRight = true;

    #endregion

    /// <summary>
    /// Gets references to rigidbody and CharacterStats script
    /// </summary>
    protected virtual void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        characterStats = GetComponent<CharacterStats>();
    }

    /// <summary>
    /// Moves character and if needed flips character sprite
    /// </summary>
    /// <param name="horizontalAxis">Movement direction</param>
    public void MoveCharacter(float horizontalAxis)
    {
        MoveOnlyCharacter(horizontalAxis);
        FlipSprite(horizontalAxis);
    }

    /// <summary>
    /// Moves character
    /// </summary>
    /// <param name="horizontalAxis">Movement direction</param>
    public void MoveOnlyCharacter(float horizontalAxis)
    {
        movementVector.Set(Mathf.Lerp(rigidBody.velocity.x, horizontalAxis * characterStats.movementSpeedGrounded, Time.deltaTime * movingSpeedSmoothen), rigidBody.velocity.y);
        
        rigidBody.velocity = movementVector;       
    }

    /// <summary>
    /// Flips sprite in X axis
    /// </summary>
    /// <param name="flipDirection">Flip direction</param>
    private void FlipSprite(float flipDirection)
    {
        
        if (flipDirection < 0 && facingRight)
        {
            Vector3 currentScale = transform.localScale;
            currentScale.x = -1;
            transform.localScale = currentScale;
            facingRight = false;
        }
        else if(flipDirection > 0 && !facingRight)
        {
            Vector3 currentScale = transform.localScale;
            currentScale.x = 1;
            transform.localScale = currentScale;
            facingRight = true;
        }
    }
}
