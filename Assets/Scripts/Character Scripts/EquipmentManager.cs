﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Character equipment manager
/// </summary>
public class EquipmentManager : MonoBehaviour
{
    #region Variables

    public WeaponBase weapon;
    public JetPack jetPack;

    #endregion
}
