﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Stores character stats
/// </summary>
public class CharacterStats : MonoBehaviour
{
    #region Variables

    public int maxHealth = 100;
    public int currentHealth = 100;

    public float movementSpeedGrounded = 10f;

    public DeadBodyManager deadBodyPrefab;

    private Vector3 hitLocation = Vector3.zero;

    #endregion

    /// <summary>
    /// Sets current health amount to max health amount
    /// </summary>
    protected virtual void Start()
    {
        currentHealth = maxHealth;
    }

    /// <summary>
    /// Subtracts health if health pool is lower than 0 activates dead body
    /// </summary>
    /// <param name="amount">Amount to subtract</param>
    public void SubtractHealth(int amount)
    {
        currentHealth -= amount;

        if(currentHealth == 0)
        {
            DeadBodyManager deadBody = (DeadBodyManager) Instantiate(deadBodyPrefab, transform.position, Quaternion.identity);
            deadBody.ActivateDeadBody(hitLocation);

            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Sets the location there the character was hit
    /// </summary>
    /// <param name="location">Hit location</param>
    public void SetHitLocation(Vector3 location)
    {
        hitLocation = location;
    }
}
