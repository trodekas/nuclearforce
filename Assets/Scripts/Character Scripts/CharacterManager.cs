﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Character manager mainly used for emitting bullet hit particles 
/// </summary>
public class CharacterManager : MonoBehaviour
{
    #region Variables

    public ParticleSystemManager onBulletHitParticles;
    private Transform onBulletHitParticlesTransfom;
    private float onBulletHitParticlesOffset = 90f;

    private float facingRightBulletParticleOffset;
    private float facingLeftBulletParticleOffset;

    private CharacterMovement characterMovement;

    #endregion

    /// <summary>
    /// Gets reference to CharacterMovement script and particle system transform
    /// </summary>
    void Awake()
    {
        onBulletHitParticlesTransfom = onBulletHitParticles.GetComponent<Transform>();
        characterMovement = GetComponent<CharacterMovement>();
    }

    /// <summary>
    /// Sets offset to particle system
    /// </summary>
    void Start()
    {
        facingRightBulletParticleOffset = onBulletHitParticlesOffset;
        facingLeftBulletParticleOffset = -1 * onBulletHitParticlesOffset;
    }

    /// <summary>
    /// Emits particles from the particle system
    /// </summary>
    /// <param name="amount">The amount of particles</param>
	public void EmitBulletHitParticles(int amount)
    {
        onBulletHitParticles.EmitFromAll(amount);
    }

    /// <summary>
    /// Emits particles from the particle system, particles will face towards the awaker
    /// </summary>
    /// <param name="awakerPosition">Awaker position</param>
    /// <param name="amount">Amount of particles emitted</param>
    public void EmitBulletHitParticles(Vector3 awakerPosition, int amount)
    {
        Vector3 deltaPosition = transform.position - awakerPosition;
        if (characterMovement.facingRight)
        {
            onBulletHitParticlesTransfom.rotation = Quaternion.LookRotation(deltaPosition) * Quaternion.AngleAxis(facingRightBulletParticleOffset, Vector3.up);
        }
        else
        {
            onBulletHitParticlesTransfom.rotation = Quaternion.LookRotation(deltaPosition) * Quaternion.AngleAxis(facingLeftBulletParticleOffset, Vector3.up);
        }

        onBulletHitParticles.EmitFromAll(amount);
    }
}
