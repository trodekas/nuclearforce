﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Manages the dead body of the character
/// </summary>
public class DeadBodyManager : MonoBehaviour
{
    #region Variables

    public Rigidbody2D[] bodyParts;
    public ParticleSystem deahtParticles;
    public int deathParticlesAmount = 30;

    public float minForceMulti = 2f;
    public float maxForceMulti = 10f;

    #endregion

    /// <summary>
    /// Actives the dead body and adds force on every part of the body
    /// </summary>
    /// <param name="hitLocation">Hit location</param>
    public void ActivateDeadBody(Vector3 hitLocation)
    {
        deahtParticles.Emit(deathParticlesAmount);

        for (int i = 0; i < bodyParts.Length; i++)
        {
            bodyParts[i].AddForce((transform.position - hitLocation) * Random.Range(minForceMulti, maxForceMulti), ForceMode2D.Impulse);

            if(transform.position.x < hitLocation.x)
            {
                bodyParts[i].AddTorque(Random.Range(minForceMulti, maxForceMulti), ForceMode2D.Impulse);
            }
            else
            {
                bodyParts[i].AddTorque(-Random.Range(minForceMulti, maxForceMulti), ForceMode2D.Impulse);
            }
        }
    }
}
