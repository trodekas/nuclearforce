﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Stores the stats of the player
/// </summary>
public class PlayerStats : CharacterStats
{
    #region Variables

    public float jetPackRechargeMultiplier = 0.5f;

    private EquipmentManager equipmentManager;
    private StatsBarManager statsBarManager;
    private GameManager gameManager;

    #endregion

    /// <summary>
    /// Gets references to object
    /// </summary>
    void Awake()
    {
        equipmentManager = GetComponent<EquipmentManager>();
        statsBarManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<StatsBarManager>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    /// <summary>
    /// Calls base Start() and set jet pack recharge multiplier
    /// </summary>
    protected override void Start()
    {
        base.Start();

        equipmentManager.jetPack.SetJetPackRechargeMultiplier(jetPackRechargeMultiplier);
    }

    /// <summary>
    /// Updates UI of the health bar
    /// </summary>
    void Update()
    {
        statsBarManager.SetHealthBarAmount(currentHealth);
    }

    /// <summary>
    /// Handles the disable of the gameObject, activates dead UI and set the health bar to 0
    /// </summary>
    void OnDisable()
    {
        statsBarManager.SetHealthBarAmount(0);
        gameManager.ActiveDeadUI();
    }
}
