﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Manages UI stats bars
/// </summary>
public class StatsBarManager : MonoBehaviour
{
    #region Variables

    public Transform healthBar;
    public Transform jetPackBar;

    private PlayerStats playerStats;
    private JetPack jetPack;

    #endregion

    /// <summary>
    /// Gets references to player stats and jet pack scripts
    /// </summary>
    void Awake()
    {
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        jetPack = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<JetPack>();
    }
	
    /// <summary>
    /// Set health bar amount
    /// </summary>
    /// <param name="newHealthAmount">New health bar amount</param>
    public void SetHealthBarAmount(float newHealthAmount)
    {
        if(newHealthAmount < 0)
        {
            healthBar.localScale = new Vector3(0f, 1f, 1f);
        }
        else
        {
            if(healthBar != null)
            {
                healthBar.localScale = new Vector3((newHealthAmount * 10) / playerStats.maxHealth, 1f, 1f);
            }
        }    
    }

    /// <summary>
    /// Set jet pack bar amount
    /// </summary>
    /// <param name="newJetPackAmount">New jet pack bar amount</param>
    public void SetJetPackBarAmount(float newJetPackAmount)
    {
        jetPackBar.localScale = new Vector3((newJetPackAmount * 10) / jetPack.maxJetPackFuelTime, 1f, 1f);
    }
}
