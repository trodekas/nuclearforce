﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles player movement
/// </summary>
public class PlayerMovement : CharacterMovement
{
    #region Variables

    public Vector3 additionalCameraOffset = new Vector3(2f, 0f, 0f);
    private Vector3 cameraOffset;
    private bool previousFacing = true;

    private CameraFollow cameraScript;

    public Transform groundCheckEnd;
    public LayerMask groundCheckCollisionLayer;
    public bool grounded = false;

    private EquipmentManager equipmentManager;

    private AnimationController playerAnimationController;

    #endregion

    /// <summary>
    /// Gets references to objects
    /// </summary>
    protected override void Awake()
    {
        cameraScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFollow>();
        equipmentManager = GetComponent<EquipmentManager>();
        playerAnimationController = GetComponent<AnimationController>();

        base.Awake();
    }

    /// <summary>
    /// Sets camera offset
    /// </summary>
    void Start()
    {
        cameraOffset = cameraScript.GetOffset() + additionalCameraOffset;
        cameraScript.SetOffset(cameraOffset);
    }

    /// <summary>
    /// Checks if the player is grounded repeatably
    /// </summary>
    void FixedUpdate()
    {
        GroundCheck();
    }

    /// <summary>
    /// Checks if the player is grounded
    /// </summary>
    private void GroundCheck()
    {
        if (Physics2D.Linecast(transform.position, groundCheckEnd.position, groundCheckCollisionLayer))
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }

        playerAnimationController.SetGrounded(grounded);
    }

    /// <summary>
    /// Moves the player and changes the camera offset
    /// </summary>
    /// <param name="horizontalAxis">Movement direction</param>
    public void MovePlayer(float horizontalAxis)
    {
        MoveCharacter(horizontalAxis);
        ChangeCameraOffset(horizontalAxis);
    }

    /// <summary>
    /// Changes camera offset according if the direction of traveling was changed
    /// </summary>
    /// <param name="horizontalAxis">Movement direction</param>
    private void ChangeCameraOffset(float horizontalAxis)
    {
        if (horizontalAxis < 0 && previousFacing)
        {
            cameraOffset.x *= -1;
            cameraScript.SetOffset(cameraOffset);
            previousFacing = false;
           
        }
        else if (horizontalAxis > 0 && !previousFacing)
        {
            cameraOffset.x *= -1;
            cameraScript.SetOffset(cameraOffset);
            previousFacing = true;
        }
    }

    /// <summary>
    /// Activates the jet pack
    /// </summary>
    public void MakeJumpAction()
    {
        if (equipmentManager.jetPack.currentJetPackFuelTime > 0)
        {
            equipmentManager.jetPack.Propulse();
        }
    }
}
