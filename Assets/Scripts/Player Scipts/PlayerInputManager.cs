﻿using UnityEngine;
using CnControls;
using System.Collections;

/// <summary>
/// Player input manager
/// </summary>
public class PlayerInputManager : MonoBehaviour
{
    #region Variables

    private PlayerMovement playerMovement;
    private EquipmentManager equipmentManager;

    private AnimationController playerAnimationController;
    private CameraShake cameraShake;

    #endregion

    /// <summary>
    /// Gets references to objects
    /// </summary>
    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
        equipmentManager = GetComponent<EquipmentManager>();
        playerAnimationController = GetComponent<AnimationController>();
        cameraShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInChildren<CameraShake>();
    }

    /// <summary>
    /// Checks for player input in every frame
    /// </summary>
    void Update ()
    {
        if(CnInputManager.GetButtonUp("Jump"))
        {
            equipmentManager.jetPack.SetUsingJetPack(false);
        }
        if(CnInputManager.GetButton("Fire1"))
        {
            equipmentManager.weapon.Attack();
            playerAnimationController.SetShooting(true);
            cameraShake.Shake();
        }
        else
        {
            playerAnimationController.SetShooting(false);
        }
    }

    /// <summary>
    /// Get movement direction input and moves the character
    /// </summary>
    void FixedUpdate()
    {
        float horizontalAxis = CnInputManager.GetAxisRaw("Horizontal");
        playerMovement.MovePlayer(horizontalAxis);
        playerAnimationController.SetVelocity(Mathf.Abs(horizontalAxis));

#if UNITY_ANDROID && !UNITY_EDITOR
        if (!playerMovement.grounded)
        {
            //playerMovement.MoveOnlyCharacter(LowPassFilterAccelerometer().x * 20);
            playerMovement.MoveOnlyCharacter(Mathf.Clamp(Input.acceleration.x * 10, -1, 1));
        }
#endif
        if (CnInputManager.GetButton("Jump"))
        {
            playerMovement.MakeJumpAction();
            equipmentManager.jetPack.SetUsingJetPack(true);
        }
    }
}
