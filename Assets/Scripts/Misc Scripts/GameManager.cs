﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;

/// <summary>
/// Game manager class used for tracking and changing game's states
/// </summary>
public class GameManager : MonoBehaviour
{
    #region Variables

    public int score = 0;
    public int maxTime = 60;
    private float dTime = 0;
    private int currentTime = 60;

    public Text scoreText;
    public Text timeText;

    public Transform gamePlayUI;
    public GameObject healthBar;
    public GameObject jetPackBar;

    public GameObject deathOverlay;

    public GameObject gamesEndUI;
    public GameObject deadUI;
    public GameObject timesUpUI;
    public Text gameOverScoreText;
    public Text gameOverTimeText;

    public DatabaseManager database;

    private bool goingToMainMenu = false;

    #endregion

    /// <summary>
    /// Awake method use for setting up time and score text fields
    /// also for making a progression event for GameAnalytics
    /// </summary>
    void Awake()
    {
        scoreText.text = "Score " + score.ToString();
        timeText.text = "Time " + maxTime.ToString();

        currentTime = maxTime;

        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Purple world", "Level 1", "Started");
    }

    /// <summary>
    /// Used for increasing the score of the player by one also updates score text field
    /// </summary>
    public void IncreaseScore()
    {
        score++;
        if(scoreText != null)
        {
            scoreText.text = "Score " + score.ToString();
        }
    }

    /// <summary>
    /// Ticks every frame used for updating game's timer
    /// </summary>
    void Update()
    {
        dTime += Time.deltaTime;
        if(dTime > 1f)
        {
            dTime = 0;
            currentTime--;
            timeText.text = "Time " + currentTime.ToString();

            if(currentTime == 0)
            {
                ActiveTimesUpUI();
            }
        }
    }

    /// <summary>
    /// Activates UI which indicates that the playes has died
    /// </summary>
    public void ActiveDeadUI()
    {
        if(!goingToMainMenu)
        {
            ChangeUI();
            deadUI.SetActive(true);

            gameOverScoreText.text = "Score " + score.ToString();
            gameOverTimeText.text = "Time alive " + (maxTime - currentTime).ToString();
        } 
    }

    /// <summary>
    /// Activates UI which indicates that the player ran out of time
    /// </summary>
    public void ActiveTimesUpUI()
    {
        if(!goingToMainMenu)
        {
            ChangeUI();
            timesUpUI.SetActive(true);

            gameOverScoreText.text = "Score " + score.ToString();
        }   
    }

    /// <summary>
    /// Changes UI if an event which requires UI change occures
    /// </summary>
    private void ChangeUI()
    {
        if(gamePlayUI != null)
        {
            gamePlayUI.gameObject.SetActive(false);
        }
        
        if(healthBar != null)
        {
            healthBar.SetActive(false);
        }
        if(jetPackBar != null)
        {
            jetPackBar.SetActive(false);
        }       

        if(deathOverlay != null)
        {
            deathOverlay.SetActive(true);
        }
       
        if(gamesEndUI != null)
        {
            gamesEndUI.SetActive(true);
        }

        if(!goingToMainMenu)
        {
            database.InsertHighScore(score, (maxTime - currentTime));
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Purple world", "Level 1", "Completed", score);
        }
    }

    /// <summary>
    /// Handles retry button press (realoads current scene)
    /// </summary>
    public void RetryButtonAction()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.buildIndex);
    }

    /// <summary>
    /// Handles main menu button press (goes to main menu)
    /// </summary>
    public void MainMenuButtonAction()
    {
        goingToMainMenu = true;
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Purple world", "Level 1", "Exited", currentTime);

        SceneManager.LoadScene(0);
    }
}
