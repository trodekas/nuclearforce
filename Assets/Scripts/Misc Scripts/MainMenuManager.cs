﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using GameAnalyticsSDK;

/// <summary>
/// Class for managing main menu actions
/// </summary>
public class MainMenuManager : MonoBehaviour
{
    #region Variables

    public int playScene;

    public GameObject mainMenu;
    public GameObject leaderboard;
    public GameObject thanksTo;

    public DatabaseManager database;
    public Text leaderboardText;

    #endregion

    /// <summary>
    /// Handles play button press (launches playble scene)
    /// </summary>
    public void PlayButtonAction()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Undefined, "Main Menu", "Play");
        SceneManager.LoadScene(playScene);
    }

    /// <summary>
    /// Handles leaderboard button press (goes to leaderboard window, gets and shows leaderboard entries)
    /// </summary>
    public void LeaderboardButtonAction()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Undefined, "Main Menu", "Leaderboard");
        mainMenu.SetActive(false);
        leaderboard.SetActive(true);

        string leaderboardEntries = database.GetHighScores();
        if(leaderboardEntries != "")
        {
            string header = string.Format("{0, -12:s}{1, -12:s}{2, -12:s}", "No.", "Score", "Time");
            header += '\n';
            leaderboardText.text = header + leaderboardEntries;
        }
        else
        {
            leaderboardText.text = "Leaderboard is empty";
        }
    }

    /// <summary>
    /// Handles back button press (goes from leaderboard window to main menu or from thanks to window to main menu)
    /// </summary>
    public void BackButtonAction()
    {
        if(leaderboard.activeInHierarchy)
        {
            leaderboard.SetActive(false);
        }
        else
        {
            thanksTo.SetActive(false);
        }
        mainMenu.SetActive(true);
    }

    /// <summary>
    /// Handles exit button press (closes the application)
    /// </summary>
    public void ExitButtonAction()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Undefined, "Main Menu", "Exited");
        Application.Quit();
    }

    /// <summary>
    /// Handles thanks to button action (goes to thanks to window)
    /// </summary>
    public void ThanksToButtonAction()
    {
        thanksTo.SetActive(true);
        mainMenu.SetActive(false);
    }
}
