﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Paricles system manager used for managing nested particle systems
/// </summary>
public class ParticleSystemManager : MonoBehaviour
{
    #region Variables

    private ParticleSystem[] childParticleSystems;

    #endregion

    /// <summary>
    /// Gets references to all nested particles systems
    /// </summary>
    void Awake()
    {
        childParticleSystems = GetComponentsInChildren<ParticleSystem>();
    }

    /// <summary>
    /// Emits particles from all managed particle systems
    /// </summary>
    /// <param name="amount">Amount particles to emit from each particle system</param>
    public void EmitFromAll(int amount)
    {
        int lenght = childParticleSystems.Length;
        for (int i = 0; i < lenght; i++)
        {
            childParticleSystems[i].Emit(amount);
        }
    }

    /// <summary>
    /// Plays all managed particle systems
    /// </summary>
    public void PlayAll()
    {
        int lenght = childParticleSystems.Length;
        for (int i = 0; i < lenght; i++)
        {
            childParticleSystems[i].Play();
        }
    }

    /// <summary>
    /// Pauses all managed particle systems
    /// </summary>
    public void PauseAll()
    {
        int lenght = childParticleSystems.Length;
        for (int i = 0; i < lenght; i++)
        {
            childParticleSystems[i].Pause();
        }
    }

    /// <summary>
    /// Stops all managed paricle systems
    /// </summary>
    public void StopAll()
    {
        int lenght = childParticleSystems.Length;
        for (int i = 0; i < lenght; i++)
        {
            childParticleSystems[i].Stop();
        }
    }
}
