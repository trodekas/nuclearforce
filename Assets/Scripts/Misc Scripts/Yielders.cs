﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Utility class for yielders used in order to reduce garbage
/// </summary>
public static class Yielders
{
    #region Variables

    static Dictionary<float, WaitForSeconds> _timeInterval = new Dictionary<float, WaitForSeconds>(100);
    static WaitForEndOfFrame _endOfFrame = new WaitForEndOfFrame();

    #endregion

    /// <summary>
    /// Waits for the end of the frame
    /// </summary>
    public static WaitForEndOfFrame EndOfFrame
    {
        get { return _endOfFrame; }
    }

    /// <summary>
    /// Waits for fixed update
    /// </summary>
    static WaitForFixedUpdate _fixedUpdate = new WaitForFixedUpdate();
    public static WaitForFixedUpdate FixedUpdate
    {
        get { return _fixedUpdate; }
    }

    /// <summary>
    /// Waits for seconds
    /// </summary>
    /// <param name="seconds">Seconds to wait</param>
    /// <returns></returns>
    public static WaitForSeconds Get(float seconds)
    {
        if (!_timeInterval.ContainsKey(seconds))
        {
            _timeInterval.Add(seconds, new WaitForSeconds(seconds));
        }
        return _timeInterval[seconds];
    }

}
