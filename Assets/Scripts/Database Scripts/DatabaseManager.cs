﻿using UnityEngine;

/// <summary>
/// Class for managing the database
/// </summary>
public class DatabaseManager : MonoBehaviour
{
    #region Variables

    public static DatabaseManager Instance = null;

    private const string SQL_DB_NAME = "leaderboard.db";

    private DataService dataService;

    #endregion

    /// <summary>
    /// Opens data service
    /// </summary>
    void Awake()
    {
        dataService = new DataService(SQL_DB_NAME);
        dataService.CreateDB();
    }

    /// <summary>
    /// Closes database on destroy
    /// </summary>
    void OnDestroy()
    {
        SQLiteClose();
    }

    /// <summary>
    /// Inserts data into database
    /// </summary>
    /// <param name="score">Player's score</param>
    /// <param name="time">Time</param>
    public void InsertHighScore(int score, int time)
    {
        dataService.InsertHighscoreEntry(score, time);
    }

    /// <summary>
    /// Closes database connection
    /// </summary>
    private void SQLiteClose()
    {
        dataService.CloseConnection();
    }

    /// <summary>
    /// Return 5 top highscores from database
    /// </summary>
    /// <returns>String of 5 top highscores</returns>
    public string GetHighScores()
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        var entries = dataService.GetHighScores();
        int place = 1;
        foreach (var item in entries)
        {
            sb.Append(string.Format("{0, -20}{1}", place, item.ToString()));
            sb.AppendLine();
            place++;

            if(place == 6)
            {
                break;
            }
        }

        return sb.ToString();
    }
}
