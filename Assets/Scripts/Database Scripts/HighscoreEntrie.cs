﻿using SQLite4Unity3d;

/// <summary>
/// Class for highscore entry
/// </summary>
public class HighscoreEntrie
{

    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public int Score { get; set; }
    public int Time { get; set; }

    public override string ToString()
    {
        return string.Format("{0, -25:D}{1, -20:D}", Score, Time);
    }
}
