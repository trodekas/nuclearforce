﻿using SQLite4Unity3d;
using UnityEngine;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

/// <summary>
/// Class for database data service
/// </summary>
public class DataService
{

    #region Variables

    private SQLiteConnection mConnection;

    #endregion

    /// <summary>
    /// Creates a database if it does not exists in persistant folder
    /// </summary>
    /// <param name="DatabaseName">The name of database</param>
    public DataService(string DatabaseName)
    {

#if UNITY_EDITOR
        var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif
        mConnection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);
        Debug.Log("Final PATH: " + dbPath);

    }

    /// <summary>
    /// Creates highscores table
    /// </summary>
    public void CreateDB()
    {
        mConnection.CreateTable<HighscoreEntrie>();
    }

    /// <summary>
    /// Returns all highscores from database, sorts them by score and time
    /// </summary>
    /// <returns>Highscores entries</returns>
    public IEnumerable<HighscoreEntrie> GetHighScores()
    {
        return mConnection.Table<HighscoreEntrie>().OrderByDescending(x => x.Score).OrderBy(y => y.Time);
    }

    /// <summary>
    /// Inserts a highscore into database
    /// </summary>
    /// <param name="score">Score of the player</param>
    /// <param name="time">Time</param>
    public void InsertHighscoreEntry(int score, int time)
    {
        var entry = new HighscoreEntrie
        {
            Score = score,
            Time = time,
        };
        mConnection.Insert(entry);
    }

    /// <summary>
    /// Closes database connection
    /// </summary>
    public void CloseConnection()
    {
        if(mConnection != null)
        {
            mConnection.Close();
            mConnection = null;
        }
    }
}
