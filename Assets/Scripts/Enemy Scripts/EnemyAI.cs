﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enemy AI script
/// </summary>
public class EnemyAI : MonoBehaviour
{
    #region Variables

    private CharacterMovement characterMovement;

    public Transform leftDomainLimit;
    public Transform rightDomaainLimit;
    public float currentIdleTarget;

    public float reactionTime = 0.2f;
    public float minWaitTime = 0.5f;
    public float maxWaitTime = 2f;
    public bool playerInSight = false;
    public float sameHeightMargin = 2f;

    public Transform sightOriginTransform;
    public float maxDistanceToPlayer = 20f;
    public LayerMask canSightLayerMask;
    private RaycastHit2D sightRayHit;

    private Transform playerTransform;

    private AnimationController enemyAnimationController;
    private EquipmentManager equipmentManager;
    private EnemySpawner mySpawner;
    private CharacterStats stats;

    #endregion

    /// <summary>
    /// Gets references to objects
    /// </summary>
    void Awake()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        characterMovement = GetComponent<CharacterMovement>();
        enemyAnimationController = GetComponent<AnimationController>();
        equipmentManager = GetComponentInChildren<EquipmentManager>();
        stats = GetComponent<CharacterStats>();
    }

    /// <summary>
    /// Excecutes enemy action depending the the player is in sight or not
    /// </summary>
    void Update()
    {
        if(playerInSight)
        {
            characterMovement.MoveCharacter(0);
            enemyAnimationController.SetVelocity(0);

            faceTowardsPlayer();
            if(IsPlayerInSameHeight())
            {
                enemyAnimationController.SetShooting(true);
                CreateShootInput();
            }
            else
            {
                enemyAnimationController.SetShooting(false);
            }  
        }
        else if (Mathf.Abs(transform.position.x - currentIdleTarget) < 0.5)
        {
            characterMovement.MoveCharacter(0);
            enemyAnimationController.SetVelocity(0);
            enemyAnimationController.SetShooting(false);

            Invoke("SetNewPositionInDomain", Random.Range(minWaitTime, maxWaitTime));
        }
        else
        {
            CreateMovementInput();
            enemyAnimationController.SetShooting(false);
        }
    }

    /// <summary>
    /// Checks if player in sight repeatably
    /// </summary>
    /// <returns></returns>
    IEnumerator RepeatableActions()
    {
        while (this.enabled)
        {
            CheckIfPlayerInSight();

            yield return new WaitForSeconds(reactionTime);
        }
    }

    /// <summary>
    /// Check if player is in sight of the enemy
    /// </summary>
    private void CheckIfPlayerInSight()
    {
        if(Vector3.Distance(transform.position, playerTransform.position) < maxDistanceToPlayer)
        {
            if (sightRayHit = Physics2D.Raycast(sightOriginTransform.position, playerTransform.position - transform.position, maxDistanceToPlayer, canSightLayerMask))
            {
                if(sightRayHit.collider.gameObject.CompareTag("Player"))
                {
                    playerInSight = true;
                }
                else
                {
                    playerInSight = false;
                }
            }
        }
        else
        {
            playerInSight = false;
        }
    }

    /// <summary>
    /// Sets new position in area in which enemy can walk
    /// </summary>
    private void SetNewPositionInDomain()
    {
        currentIdleTarget = Random.Range(leftDomainLimit.position.x, rightDomaainLimit.position.x);
        CancelInvoke();
    }

    /// <summary>
    /// Is current movement target on the right
    /// </summary>
    /// <returns>Is it on right?</returns>
    private bool GoRight()
    {
        return currentIdleTarget > transform.position.x;
    }

    /// <summary>
    /// Creates input in order to move the character
    /// </summary>
    private void CreateMovementInput()
    {
        if (GoRight())
        {
            characterMovement.MoveCharacter(1);
            enemyAnimationController.SetVelocity(1);
        }
        else
        {
            characterMovement.MoveCharacter(-1);
            enemyAnimationController.SetVelocity(1);
        }
    }

    /// <summary>
    /// Creates shooting in order to shoot
    /// </summary>
    private void CreateShootInput()
    {
        equipmentManager.weapon.Attack();
    }

    /// <summary>
    /// Faces the enemy towards the player
    /// </summary>
    private void faceTowardsPlayer()
    {
        if(playerTransform.position.x > transform.position.x && !characterMovement.facingRight)
        {
            characterMovement.MoveCharacter(1);
        }
        else if(playerTransform.position.x < transform.position.x && characterMovement.facingRight)
        {
            characterMovement.MoveCharacter(-1);
        }
    }

    /// <summary>
    /// Checks if the player is roughly on the same height
    /// </summary>
    /// <returns>Is the plyer on the same height?</returns>
    private bool IsPlayerInSameHeight()
    {
        return Mathf.Abs(transform.position.y - playerTransform.position.y) < sameHeightMargin;  
    }

    /// <summary>
    /// Sets the spawn point of the enemy
    /// </summary>
    /// <param name="spawner"></param>
    public void SetEnemySpawner(EnemySpawner spawner)
    {
        mySpawner = spawner;
    }

    /// <summary>
    /// Handles the disable of the enemy, canles the invoke and decreases the spawner's active enemy counter
    /// </summary>
    void OnDisable()
    {
        CancelInvoke();

        mySpawner.DecreaseActiveEnemies();
    }

    /// <summary>
    /// Handles on enable
    /// </summary>
    void OnEnable()
    {
        SetNewPositionInDomain();
        StartCoroutine(RepeatableActions());
        stats.currentHealth = stats.maxHealth;
    }
}
