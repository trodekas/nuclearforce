﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enemy spawner
/// </summary>
public class EnemySpawner : MonoBehaviour
{
    #region Variables

    public Transform leftDomainLimit;
    public Transform rightDomainLimit;

    public EnemyAI enemy;

    public int poolSize = 5;
    private EnemyAI[] pooledEnemies;
    private int activeEnemies = 0;

    public Transform playerTransform;
    public float safeToSpawnDistance = 10f;

    private GameManager gameManager;

    #endregion

    /// <summary>
    /// Gets reference to GameManager and creates the pool of enemies
    /// </summary>
    void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();

        pooledEnemies = new EnemyAI[poolSize];
        for (int i = 0; i < poolSize ; i++)
        {
            EnemyAI temp = Instantiate(enemy) as EnemyAI;
            temp.leftDomainLimit = leftDomainLimit;
            temp.rightDomaainLimit = rightDomainLimit;
            temp.SetEnemySpawner(this);
            temp.gameObject.SetActive(false);
            pooledEnemies[i] = temp;
        }
    }

    /// <summary>
    /// Spawns enemies from the pool if not all the enemies are active and the player is far enough on the X axis
    /// </summary>
    void Update()
    {
        if (activeEnemies < poolSize && Mathf.Abs(transform.position.x - playerTransform.position.x) > safeToSpawnDistance)
        {
            for (int i = 0; i < poolSize; i++)
            {
                if (!pooledEnemies[i].gameObject.activeInHierarchy)
                {
                    Vector2 spawnLocation = new Vector2(Random.Range(leftDomainLimit.position.x, rightDomainLimit.position.x), transform.position.y);
                    pooledEnemies[i].transform.position = spawnLocation;
                    pooledEnemies[i].gameObject.SetActive(true);

                    activeEnemies++;
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Decreases active enemies counter by one
    /// </summary>
    public void DecreaseActiveEnemies()
    {
        activeEnemies--;
        gameManager.IncreaseScore();
    }
}
