﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for shaking the camera, uses random position in unit sphere
/// </summary>
public class CameraShake : MonoBehaviour
{
    #region Variables

    private Transform camTransform;

    private float shakeDuration = 0f;
    public float shakeSpan = 0.5f;

    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    private Vector3 originalPos;

    #endregion

    /// <summary>
    /// Sets camera transform reference
    /// </summary>
    void Awake()
    {     
        camTransform = GetComponent(typeof(Transform)) as Transform;
    }

    /// <summary>
    /// Sets original position depending on parent gameObject's local position
    /// </summary>
    void OnEnable()
    {
        originalPos = camTransform.localPosition;
    }

    /// <summary>
    /// Shakes the parent if shake duration is more than 0
    /// </summary>
    void Update()
    {
        if (shakeDuration > 0)
        {
            camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            camTransform.localPosition = originalPos;
        }
    }

    /// <summary>
    /// Sets shake duration
    /// </summary>
    public void Shake()
    {
        this.shakeDuration = shakeSpan;
    }
}
