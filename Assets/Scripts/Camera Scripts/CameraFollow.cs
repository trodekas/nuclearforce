﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for following transform with offset
/// </summary>
public class CameraFollow : MonoBehaviour
{
    #region Variables

    public Transform target;            // The position that that camera will be following.
    public float smoothing = 5f;        // The speed with which the camera will be following.

    Vector3 offset;                     // The initial offset from the target.

    #endregion

    /// <summary>
    /// Gets player transform if it is null and sets offset according to camera position
    /// </summary>
    void Start()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }

        // Calculate the initial offset.
        offset = transform.position - target.position;
    }

    /// <summary>
    /// Updates gameObject position
    /// </summary>
    void Update()
    {
        // Create a postion the camera is aiming for based on the offset from the target.
        Vector3 targetCamPos = target.position + offset;

        // Smoothly interpolate between the camera's current position and it's target position.
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
    }

    /// <summary>
    /// Sets follow offset
    /// </summary>
    /// <param name="newOffset">Offset amount</param>
    public void SetOffset(Vector3 newOffset)
    {
        offset = newOffset;
    }

    /// <summary>
    /// Retuns current follow offset
    /// </summary>
    /// <returns>Offset</returns>
    public Vector3 GetOffset()
    {
        return offset;
    }
    
}
