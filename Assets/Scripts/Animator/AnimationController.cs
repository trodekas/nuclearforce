﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class for controlling animator states
/// </summary>
public class AnimationController : MonoBehaviour
{
    #region Variables

    private Animator animator;

    int velocityHash = Animator.StringToHash("velocity");
    int shootingHash = Animator.StringToHash("shooting");
    int groundedHash = Animator.StringToHash("isGrounded");

    #endregion

    /// <summary>
    /// Gets referance to animator attached to gameObject
    /// </summary>
    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    /// <summary>
    /// Sets velocity property in animator
    /// </summary>
    /// <param name="velocity">Velocity amount</param>
    public void SetVelocity(float velocity)
    {
        animator.SetFloat(velocityHash, velocity);
    }

    /// <summary>
    /// Sets shooting property in animator
    /// </summary>
    /// <param name="shooting">Is shooting?</param>
    public void SetShooting(bool shooting)
    {
        animator.SetBool(shootingHash, shooting);
    }

    /// <summary>
    /// Sets grounded property in animator
    /// </summary>
    /// <param name="isGrounded">Is grounded?</param>
    public void SetGrounded(bool isGrounded)
    {
        animator.SetBool(groundedHash, isGrounded);
    }
}
