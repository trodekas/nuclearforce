﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Abstract class for the weapon every weapon should derive from this
/// </summary>
abstract public class WeaponBase : MonoBehaviour
{
    #region Variables

    public float fireRate = 0.5f;
    protected float timeTillLastFire = 0f;

    #endregion

    /// <summary>
    /// Updates the time till the last shot was shot
    /// </summary>
    void Update()
    {
        timeTillLastFire += Time.deltaTime;
    }

    /// <summary>
    /// Abstract method for attacking with the weapon, derived class must override it
    /// </summary>
    public abstract void Attack();
}
