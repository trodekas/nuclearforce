﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Automatic weapon script
/// </summary>
public class AutomaticWeapon : WeaponBase
{
    #region Variables

    private ParticleSystemManager particleSystemManager;

    public float accuracy = 0.5f;
    private Quaternion directionToShoot;

    public float projectileSpeed = 25f;
    public float projectileLifeTime = 1f;
    public Rigidbody2D projectile;
    public Transform shootingPoint;

    public int poolSize = 50;
    private Rigidbody2D[] pooledProjectiles;

    private CharacterMovement characterMovement;

    public ForceMode2D recoilForceMode = ForceMode2D.Force;
    public float recoilStrenghtOnGround = 10f;
    public float recoilStrenghtInAir = 20f;

    #endregion

    /// <summary>
    /// Gets references to weapon particle system manager and character movement script
    /// </summary>
    void Awake()
    {
        particleSystemManager = GetComponentInChildren<ParticleSystemManager>();
        characterMovement = GetComponentInParent<CharacterMovement>();
    }

    /// <summary>
    /// Creates the pool of bullets
    /// </summary>
    void Start()
    {
        AllocatePool();
    }

    /// <summary>
    /// Creates the pool of the bullets as rigidbodies
    /// </summary>
    public void AllocatePool()
    {
        pooledProjectiles = new Rigidbody2D[poolSize];
        for (int i = 0; i < poolSize; i++)
        {
            Rigidbody2D temp = Instantiate(projectile) as Rigidbody2D;
            pooledProjectiles[i] = temp;
            temp.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Shoots from the weapon if enough time passed from the last shot
    /// </summary>
    public override void Attack()
    {
        if (timeTillLastFire >= fireRate)
        {
            particleSystemManager.EmitFromAll(1);
            timeTillLastFire = 0f;

            Shoot();
        }
    }

    /// <summary>
    /// Gets the bullet from the pool and shoots it
    /// </summary>
    private void Shoot()
    {
        for (int i = 0; i < poolSize; i++)
        {
            if (!pooledProjectiles[i].gameObject.activeInHierarchy)
            {
                pooledProjectiles[i].transform.position = shootingPoint.position;

                if (characterMovement.facingRight)
                {
                    SetRotationAndActivateProjectile(0f, i);
                    Recoil(Vector2.left);
                }
                else
                {
                    SetRotationAndActivateProjectile(180f, i);
                    Recoil(Vector2.right);
                }

                break;
            }
        }
    }

    /// <summary>
    /// Sets projectile rotation and position
    /// </summary>
    /// <param name="rotationY">Rotation of the projectile in Y axis</param>
    /// <param name="place">Location of the projectile</param>
    private void SetRotationAndActivateProjectile(float rotationY, int place)
    {
        Quaternion newRotation = pooledProjectiles[place].transform.rotation;
        newRotation.eulerAngles = new Vector3(0f, rotationY, Random.Range(-accuracy, accuracy));
        pooledProjectiles[place].transform.rotation = newRotation;

        pooledProjectiles[place].gameObject.SetActive(true);
        Vector2 newVelocity = pooledProjectiles[place].transform.right * projectileSpeed + new Vector3(characterMovement.rigidBody.velocity.x, 0f);
        pooledProjectiles[place].velocity = newVelocity;
    }

    /// <summary>
    /// Simulates the recoil of the gun
    /// </summary>
    /// <param name="recoilDirection">The direction of the recoil</param>
    private void Recoil(Vector2 recoilDirection)
    {   
        characterMovement.rigidBody.AddForce(recoilDirection * recoilStrenghtOnGround, ForceMode2D.Force);     
    }
}

