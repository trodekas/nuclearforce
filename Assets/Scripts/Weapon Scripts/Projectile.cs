﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the bullet projectile
/// </summary>
public class Projectile : MonoBehaviour
{
    #region Variables

    public int minBloodAmount = 3;
    public int maxBloodAmount = 5;

    public float projectileLifeTime = 2f;
    public float onDestroyLifeTime = 0.2f;

    public GameObject projectile;
    public GameObject onDestroyEffect;

    private Rigidbody2D rigidBody;

    #endregion

    /// <summary>
    /// Gets reference to rigidbody
    /// </summary>
    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// Disables the projectile
    /// </summary>
    private void DisableProjectile()
    {     
        projectile.SetActive(false);

        rigidBody.velocity = Vector2.zero;

        onDestroyEffect.SetActive(true);

        Invoke("Disable", onDestroyLifeTime);
    }

    /// <summary>
    /// Handles the disable of the projectile
    /// </summary>
    private void Disable()
    {
        CancelInvoke();
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Hanldes the enable of the projectile
    /// </summary>
    void OnEnable()
    {
        onDestroyEffect.SetActive(false);
        projectile.SetActive(true);
        Invoke("DisableProjectile", projectileLifeTime);
    }

    /// <summary>
    /// Handles the collision if the collision is between the enemy or player substracts the health of the character
    /// </summary>
    /// <param name="other">Collider</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Enemy") || other.CompareTag("Player"))
        {
            other.GetComponent<CharacterManager>().EmitBulletHitParticles(transform.position, Random.Range(minBloodAmount, maxBloodAmount));
            CharacterStats stats = other.GetComponent<CharacterStats>();
            stats.SetHitLocation(transform.position);
            stats.SubtractHealth(10);
        }

        DisableProjectile();
    }
}
