﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the jatpack
/// </summary>
public class JetPack : MonoBehaviour
{
    #region Variables

    private ParticleSystemManager particles;
    private Rigidbody2D rigidBody;

    public float jetPackForce = 1.25f;
    public ForceMode2D forceMode = ForceMode2D.Impulse;
    public Vector2 propulsionDirection = new Vector2(0f, 1f);

    public float maxJetPackFuelTime = 1f;
    public float currentJetPackFuelTime;

    private float jetPackRechargeMultiplier = 0.5f;
    private bool usingJetPack = false;

    private StatsBarManager statsBarManager;

    #endregion

    /// <summary>
    /// Gets references to objects
    /// </summary>
    void Awake()
    {
        particles = GetComponentInChildren<ParticleSystemManager>();
        rigidBody = GetComponentInParent<Rigidbody2D>();

        statsBarManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<StatsBarManager>();
    }

    /// <summary>
    /// Sets current jet pack fuel time to max fuel time
    /// </summary>
    void Start()
    {
        currentJetPackFuelTime = maxJetPackFuelTime;
    }

    /// <summary>
    /// Recharges jet pack fuel and updates UI bar
    /// </summary>
    void Update()
    {
        RechargeJetPack();
        statsBarManager.SetJetPackBarAmount(currentJetPackFuelTime);
    }

    /// <summary>
    /// Recharges the jet pack if it is not used
    /// </summary>
    private void RechargeJetPack()
    {
        if (currentJetPackFuelTime < maxJetPackFuelTime && !usingJetPack)
        {
            currentJetPackFuelTime += Time.deltaTime * jetPackRechargeMultiplier;
            if (currentJetPackFuelTime > maxJetPackFuelTime)
            {
                currentJetPackFuelTime = maxJetPackFuelTime;
            }
        }
    }

    /// <summary>
    /// Adds the force to the rigidbody in order to simulation propulsion
    /// </summary>
    public void Propulse()
    {
        rigidBody.AddForce(propulsionDirection * jetPackForce, forceMode);

        particles.EmitFromAll(3);

        DechargeJetPack();
    }

    /// <summary>
    /// Decreases current fuel time
    /// </summary>
    private void DechargeJetPack()
    {
        currentJetPackFuelTime -= Time.deltaTime;

        if(currentJetPackFuelTime < 0)
        {
            currentJetPackFuelTime = 0;
        }
    }

    /// <summary>
    /// Sets jet pack recharge multiplier
    /// </summary>
    /// <param name="newRechargeMultiplier">Multiplier</param>
    public void SetJetPackRechargeMultiplier(float newRechargeMultiplier)
    {
        jetPackRechargeMultiplier = newRechargeMultiplier;
    }

    /// <summary>
    /// Sets if jet pack is being used
    /// </summary>
    /// <param name="newUsingJetPack">Is jet pack being used?</param>
    public void SetUsingJetPack(bool newUsingJetPack)
    {
        usingJetPack = newUsingJetPack;
    }
}
